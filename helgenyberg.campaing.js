window.hnc = {
  d : '',
  u : '',

  init : function(d, u){
    console.log('HNC Script Initiated');
    this.d = d;
    this.u = u;

    this.injectCSS();
  },

  injectCSS : function(){
    var style = this.d.createElement('link');
    style.type = 'text/css';
    style.rel = 'stylesheet';
    style.async = true;
    style.onload = function(){
      console.log('HNC CSS added');
    }

    style.href = this.u + 'helgenyberg.campaing.css';
    this.d.getElementsByTagName('head')[0].appendChild(style);
  }
}
